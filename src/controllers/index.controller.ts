import { Request, Response } from 'express';
import { NotFoundError } from '../errors/not-found.error';
import { UsersModel } from '../model/users.model';
import { indexRepository, IndexRepository } from '../repository/index.repository';



export class IndexController {

    private readonly indexRepository: IndexRepository;

    constructor() {
        this.indexRepository = indexRepository;
    }

    async get(req: Request, res: Response): Promise<void> {
        console.log('in index ctrl');
        const users = await (new IndexRepository()).findAllUsers();
        console.log(`dbd response = ${users}`);
        if (!users) {
            throw new NotFoundError('Users not found.');
        }
        res.send(users);
    }
}