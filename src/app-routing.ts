import { Router } from "express";
import { IndexController } from './controllers/index.controller';

export const router = Router();
const indexController = new IndexController;

router.get('/', indexController.get)