import { Pool, PoolConfig } from 'pg';
import { db } from "../config/db";

const pool = new Pool(<PoolConfig>db);

export class IndexRepository {
    async findAllUsers(): Promise<Array<object>> {
        const sql = 'SELECT * FROM public."user"';
        const client = await pool.connect();
        const result = await pool.query(sql);
        client.release();
        return result.rows;
    }
}

export const indexRepository = new IndexRepository();