export const db = {
    database: process.env.DB_NAME || 'project-3-schedule',
    user: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASSWORD || 'project-3',
    host: process.env.DB_HOST || '127.0.0.1',
    port: process.env.DB_PORT || 5432,
};