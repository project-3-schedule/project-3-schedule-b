export class NotFoundError extends Error {
    code = 404;
}
