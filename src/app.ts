import express, { Request, Response } from "express";
import { router } from "./app-routing";

const app = express();

app.use(router);

app.use((req: Request, res: Response) => {
    res.status(404);
    res.json({ error: 'Not found'});
});

app.listen(3000, () => {
    console.log('Running on port 3000')
})
