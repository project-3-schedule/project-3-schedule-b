import { UserModel } from "./user.model";

export interface UsersModel {
    items: UserModel[]
}