CREATE TABLE IF NOT EXISTS public.teacher
(
    id integer NOT NULL,
    lastname character varying COLLATE pg_catalog."default" NOT NULL,
    firstname character varying COLLATE pg_catalog."default" NOT NULL,
    patronymic character varying COLLATE pg_catalog."default" NULL,
    CONSTRAINT teacher_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.teacher
    OWNER to postgres; 