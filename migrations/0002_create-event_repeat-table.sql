CREATE TYPE public.enum AS ENUM
    ('date', 'never', 'stop');

ALTER TYPE public.enum
    OWNER TO postgres;

CREATE TABLE IF NOT EXISTS public.event_repeat
(
    id integer NOT NULL,
    stop_date date,
    use_monday boolean,
    use_tuesday boolean,
    use_wednesday boolean,
    use_thursday boolean,
    use_friday boolean,
    use_saturday boolean,
    use_sunday boolean,
    duration time without time zone,
    stop_count integer,
    stop_type enum,
    CONSTRAINT event_repeat_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.event_repeat
    OWNER to postgres;