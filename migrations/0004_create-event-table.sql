CREATE TABLE IF NOT EXISTS public.event
(
    id integer NOT NULL,
    datetime timestamp with time zone[] NOT NULL,
    teacher_id integer NOT NULL,
    event_repeat_id integer NOT NULL,
    event_number integer NOT NULL,
    duration time without time zone NOT NULL,
    CONSTRAINT event_pkey PRIMARY KEY (id),
    CONSTRAINT fk_event_repeat FOREIGN KEY (event_repeat_id)
        REFERENCES public.event_repeat (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID,
    CONSTRAINT fk_event_teacher FOREIGN KEY (teacher_id)
        REFERENCES public.teacher (id) MATCH SIMPLE
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.event
    OWNER to postgres;
